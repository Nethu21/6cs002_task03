public class Aardvark_Controller {

	private Aardvark_Menu model;
	private Aardvark_View view;
	
	public Aardvark_Controller(Aardvark_Menu model, Aardvark_View view) {
		this.model = model;
		this.view = view;
	}
	
	public void setPlayerName(String name) {
		model.setName(name);
	}
	
	public String getPlayerName() {
		return model.getName();
	}
	
	public void updateView() {
		view.aardvarkGUI();
	}
	
}
