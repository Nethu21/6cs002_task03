
public class Aardvark_Game {

	public static void main(String[] args) {
		Aardvark_Menu model = new Aardvark_Menu();
		Aardvark_View view = new Aardvark_View();
		Aardvark_Controller controller = new Aardvark_Controller(model, view);
		controller.updateView();
	}
	
}
