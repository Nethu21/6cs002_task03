
public class Aardvark_View { 
	

	public void aardvarkGUI() {
		  Obs_Subject obs_subject = new Obs_Subject();

	      new Game_intro_Obs(obs_subject);
	      new Game_Player_Obs(obs_subject);
	      new Game_Menu_Obs(obs_subject);
	      new Hard_Levels_Obs(obs_subject);
	      
	      obs_subject.notifyAllObservers();
	}
	
} 
