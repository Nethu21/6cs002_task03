
public class CommandAardvark_Invoker {
	
	private CommandAardvark_Interface placeDomino;
	
	public CommandAardvark_Invoker(Command_PlaceDomino_A command_PlaceDomino_A) {
		this.placeDomino = command_PlaceDomino_A;
	}
	
	public void doPlaceDomino() {
		placeDomino.execute();
	}
	
	public void undoPlaceDomino() {
		placeDomino.unexecute();
	}

}
