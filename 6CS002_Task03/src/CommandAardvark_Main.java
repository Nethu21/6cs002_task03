
public class CommandAardvark_Main {

	public static void main(String[] args) {
		Aardvark ardvark = new Aardvark(1);
		
		Command_PlaceDomino_A placeDomino = new Command_PlaceDomino_A(ardvark);
		
		CommandAardvark_Invoker commandAardvark_Invoker = new CommandAardvark_Invoker(placeDomino);
		commandAardvark_Invoker.doPlaceDomino();
		
		System.out.println("\nUndo the domino placement (Y/N)?");
		String undo = IOLibrary.getString();
		if(undo.equalsIgnoreCase("Y")) {
			commandAardvark_Invoker.undoPlaceDomino();
		}
	}

}
