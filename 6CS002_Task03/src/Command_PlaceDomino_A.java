
public class Command_PlaceDomino_A implements CommandAardvark_Interface {
	
	private Aardvark aardvark;
	private int x13, y13;
	
	public Command_PlaceDomino_A(Aardvark aardvark) {
		this.aardvark = aardvark;
	}

	@Override
	public void execute() {
		System.out.println("\nPLACING DOMINO");
		System.out.println("Where will the top left of the domino be?");
        System.out.println("Column?");

        int x = AardvarkMethods.badInput(99);
        while (x < 1 || x > 8) {
          try {
            String s3 = IOLibrary.getString();
            x = Integer.parseInt(s3);
            this.x13 = Integer.parseInt(s3);
          } catch (Exception e) {
            System.out.println("Bad input");
            x = AardvarkMethods.badInput(65);
          }
        }
        System.out.println("Row?");
        int y = AardvarkMethods.badInput(98);
        while (y < 1 || y > 7) {
          try {
            String s3 = IOLibrary.getString();
            y = Integer.parseInt(s3);
            this.y13 = Integer.parseInt(s3);
          } catch (Exception e) {
            System.out.println("Bad input");
            y = AardvarkMethods.badInput(64);
          }
        }
        x--;
        y--;
        System.out.println("Horizontal or Vertical (H or V)?");
//            int y2, x2;
        Location lotion;
        while (true) {
          String s3 = IOLibrary.getString();
          if (s3 != null && s3.toUpperCase().startsWith("H")) {
            lotion = new Location(x, y, Location.DIRECTION.HORIZONTAL);
            System.out.println("Direction to place is " + lotion.d);
            aardvark.x2 = x + 1;
            aardvark.y2 = y;
            break;
          }
          if (s3 != null && s3.toUpperCase().startsWith("V")) {
            lotion = new Location(x, y, Location.DIRECTION.VERTICAL);
            System.out.println("Direction to place is " + lotion.d);
            aardvark.x2 = x;
            aardvark.y2 = y + 1;
            break;
          }
          System.out.println("Enter H or V");
        }
        if (aardvark.x2 > 7 || aardvark.y2 > 6) {
          System.out.println("Problems placing the domino with that position and direction");
        } else {
          // find which domino this could be
          Domino d = aardvark.aardvark_process.findGuessByLH(aardvark, aardvark.grid[y][x], aardvark.grid[aardvark.y2][aardvark.x2], aardvark._g);
          if (d == null) {
            System.out.println("There is no such domino");
          }
          // check if the domino has not already been placed
          if (d.placed) {
            System.out.println("That domino has already been placed :");
            System.out.println(d);
          }
          // check guessgrid to make sure the space is vacant
          if (aardvark.gg[y][x] != 9 || aardvark.gg[aardvark.y2][aardvark.x2] != 9) {
            System.out.println("Those coordinates are not vacant");
          }
          // if all the above is ok, call domino.place and updateGuessGrid
          aardvark.gg[y][x] = aardvark.grid[y][x];
          aardvark.gg[aardvark.y2][aardvark.x2] = aardvark.grid[aardvark.y2][aardvark.x2];
          if (aardvark.grid[y][x] == d.high && aardvark.grid[aardvark.y2][aardvark.x2] == d.low) {
            d.place(x, y, aardvark.x2, aardvark.y2);
          } else {
            d.place(aardvark.x2, aardvark.y2, x, y);
          }
          aardvark.score += 1000;
          aardvark.aardvark_process.collateGuessGrid(aardvark);
          aardvark.pf.dp.repaint();
        }
	}

	@Override
	public void unexecute() {
		System.out.println("\nUNDOING PLACING DOMINO");
        x13--;
        y13--;
        Domino lkj = aardvark.aardvark_process.findGuessAt(aardvark, x13, y13, aardvark._g);
        if (lkj == null) {
          System.out.println("Couln't find a domino there");
        } else {
          lkj.placed = false;
          aardvark.gg[lkj.hy][lkj.hx] = 9;
          aardvark.gg[lkj.ly][lkj.lx] = 9;
          aardvark.score -= 1000;
          aardvark.aardvark_process.collateGuessGrid(aardvark);
          aardvark.pf.dp.repaint();
          System.out.println("CommandAardvark_Interface Undone...");
        }
	}

}
