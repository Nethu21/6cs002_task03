
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Game_Menu_Obs extends Game_Observer{
	
	public Game_Menu_Obs(Obs_Subject obs_Subject){
	      this.obs_Subject = obs_Subject;
	      this.obs_Subject.attach(this);
	   }
			
	   @Override
	   public void displayFrame() {
		   mainMenuFrame.setLayout(new GridLayout(0, 1));
		   label = new JLabel(MultiLinugualStringTable.getMessage(1) + ". " + MultiLinugualStringTable.getMessage(2));
			mainMenuFrame.add(label);
		   	button = new JButton("Play");
			mainMenuFrame.add(button);
			button.addActionListener(this); 
			button = new JButton("View High Scores");
			mainMenuFrame.add(button);
			button.addActionListener(this); 
			button = new JButton("View Rules");
			mainMenuFrame.add(button);
			button.addActionListener(this); 
			button = new JButton("Quit");
			mainMenuFrame.add(button);
			button.addActionListener(this); 
			mainMenuFrame.pack();
	   }

}
