import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public abstract class Game_Observer extends JFrame implements ActionListener{
	static JTextField text; 
	static JFrame welcomeFrame = new JFrame("Aardvark");  
	static JFrame mainMenuFrame = new JFrame("Aardvark Main Menu");
	static JFrame difficultyMenuFrame = new JFrame("Aardvark Difficulty Menu");
	static JButton button; 
	static JLabel label; 
	static Aardvark_Menu model = new Aardvark_Menu();
	static Aardvark_View view = new Aardvark_View();
	static Aardvark_Controller controller = new Aardvark_Controller(model, view);
	static protected Aardvark aardvark;
	static int level;
	
	protected Obs_Subject obs_Subject;
	
	public abstract void displayFrame();

	public void actionPerformed(ActionEvent e) 
	{ 
		String s = e.getActionCommand(); 
		if (s.equals("Ok")) { 
			controller.setPlayerName(text.getText());
			aardvark.playerName = model.getName();
			welcomeFrame.dispose();
			mainMenuFrame.setVisible(true);
		} else if (s.equals("Play")){
			difficultyMenuFrame.setVisible(true);
		} else if (s.equals("View Rules")){
			aardvark.abo_gamerules();
		} else if (s.equals("View High Scores")){
			aardvark.viewHighScores();
		} else if (s.equals("Quit")){
			aardvark.exit_play();
		} else if (s.equals("Simples")){
			level = 1;
			aardvark.playGame();
		} else if (s.equals("Not-so-simples")){
			level = 2;
			aardvark.playGame();
		} else if (s.equals("Super-duper-shuffled")){
			level = 3;
			aardvark.playGame();
		}
	}

}
