
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Game_Player_Obs extends Game_Observer{
	
	   public Game_Player_Obs(Obs_Subject obs_Subject){
	      this.obs_Subject = obs_Subject;
	      this.obs_Subject.attach(this);
	   }

	   @Override
	   public void displayFrame() {
			label = new JLabel(MultiLinugualStringTable.getMessage(0));
			welcomeFrame.add(label);
			text = new JTextField(16); 
		    welcomeFrame.add(text);
		    button = new JButton("Ok");
			welcomeFrame.add(button);
			button.addActionListener(this); 
			welcomeFrame.pack();
	   }

}
