import java.awt.GridLayout;

import javax.swing.JButton;

public class Hard_Levels_Obs extends Game_Observer{
	
	public Hard_Levels_Obs(Obs_Subject obs_Subject){
	      this.obs_Subject = obs_Subject;
	      this.obs_Subject.attach(this);
	   }
			
	   @Override
	   public void displayFrame() {
		   difficultyMenuFrame.setLayout(new GridLayout(0, 1));
		   	button = new JButton("Simples");
		   	difficultyMenuFrame.add(button);
			button.addActionListener(this); 
			button = new JButton("Not-so-simples");
			difficultyMenuFrame.add(button);
			button.addActionListener(this); 
			button = new JButton("Super-duper-shuffled");
			difficultyMenuFrame.add(button);
			button.addActionListener(this); 
			difficultyMenuFrame.pack();
		   aardvark = new Aardvark(level);
	   }
	   

}
