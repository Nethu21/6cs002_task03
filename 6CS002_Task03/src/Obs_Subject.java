
import java.util.ArrayList;
import java.util.List;

public class Obs_Subject{
	
	private List<Game_Observer> game_Observers = new ArrayList<Game_Observer>();

	   public void attach(Game_Observer game_Observer){
	      game_Observers.add(game_Observer);		
	   }

	   public void notifyAllObservers(){
	      for (Game_Observer game_Observer : game_Observers) {
	         game_Observer.displayFrame();
	      }
	   } 	
	
}
