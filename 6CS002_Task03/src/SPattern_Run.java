
public class SPattern_Run {

	public static void main(String[] args) {
	      SContext sContext = new SContext(new StrategyPlay_one());		
	      sContext.executeStrategy();

	      sContext = new SContext(new StrategyPlay_two());		
	      sContext.executeStrategy();

	      sContext = new SContext(new StrategyPlay_three());		
	      sContext.executeStrategy();
	   }
	
}
