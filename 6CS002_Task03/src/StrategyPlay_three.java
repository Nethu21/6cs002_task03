public class StrategyPlay_three implements Strategy{
	
   @Override
   public void autoPlay() {
	   Aardvark aardvark = new Aardvark(3);
	   aardvark.playerName = "Level 3 Player";
	   aardvark.playGame();
   }
   
}